<script src="{{ asset('js/app.js') }}" defer></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<h3>Each Customer Order Details</h3>
<table  class="table table-bordered">
  <thead>
    <tr>
    <th scope="col">SI#</th>
    <th scope="col">Order Number</th>
    <th scope="col">Product Code</th>
    <th scope="col">Quantity Ordered</th>
    <th scope="col">Product Scale</th>
    <th scope="col">BuyPrice</th>
    <th scope="col">MSRP</th>
    <th scope="col">Product Vendor</th>
    <th scope="col">Product Description</th>
    <tr>
  </thead>
  <tbody>
  <?php 
  $i= 0;
    foreach($order_details_data as $list)
    {
        //print_r($list);
    $i++;
  ?>
    <tr>
    <td><?php echo $i;?></td>
	<td>{{$list->orderNumber}}</td>
	<td>{{$list->productCode}}</td>
      <td>{{$list->quantityOrdered}}</td>
      <td>{{$list->productScale}}</td>
      <td>{{$list->buyPrice}}</td>
      <td>{{$list->MSRP}}</td>
      <td>{{$list->productVendor}}</td>
      <td>{{$list->productDescription}}</td>
    </tr>
  <?php 
    }
  ?>
  </tbody>
</table>