<script src="{{ asset('js/app.js') }}" defer></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<h4>Customer Details</h4>
<table  class="table table-bordered">
  <thead>
    <tr>
     <th scope="col">SI#</th>
	 <th scope="col">Customer Name</th>
	 <th scope="col">Customer Phone No</th>
      <th scope="col">Order Number</th>
	  <th scope="col">Order Details</th>
	  <th scope="col">OrderDate</th>
      <th scope="col">RequiredDate</th>
      <th scope="col">shippedDate</th>
      <th scope="col">Shipped Comments</th>
     
    </tr>
  </thead>
  <tbody>
  <?php 
  $i= 0;
    foreach($users as $list)
    {
        //print_r($list);
    $i++;
  ?>
    <tr>
    <td><?php echo $i;?></td>
	<td>{{$list->customerName}}</td>
	<td>{{$list->phone}}</td>
      <td>{{$list->orderNumber}}</td>
      <td><a href="{{ url('./orderdetails/'.$list->orderNumber) }}" class="btn btn-danger">Order Details</a></td>

      <td>{{$list->orderDate}}</td>
      <td>{{$list->requiredDate}}</td>
      <td>{{$list->shippedDate}}</td>
      <td>{{$list->comments}}</td>
      
      
    </tr>
  <?php 
    }
  ?>
  </tbody>
</table>