<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //Route::POST('create',[StudInsertController::class, 'list']);
    return view('index');
});



use App\Http\Controllers\HomeController;
Route::get('order',[HomeController::class, 'listdata']);

//Route::get('/projects/display/{id}', 'ProjectsController@getDisplay');


Route::get('orderdetails/{id}',[HomeController::class, 'orderdetailsdata']);

//Route::post('create','StudInsertController@insert');
//Route::post('create', [StudInsertController::class, 'insert']);

