<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function listdata()
    {
        $users =  DB::table('customers')->select('*')->join('orders','customers.customerNumber','=','orders.customerNumber')->get();
        return view('welcome')->with('users', $users);
    }

    public function orderdetailsdata($ordernumber)
    {
        $order_details_data =  DB::table('orderdetails')->select('*')->join('products','orderdetails.productCode','=','products.productCode')->where(['orderdetails.ordernumber' => $ordernumber])->get();
       
       
        return view('orderdetailsdata')->with('order_details_data', $order_details_data);
    }
}
